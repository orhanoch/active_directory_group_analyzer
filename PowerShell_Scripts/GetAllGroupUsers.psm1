$ScriptDir = Split-Path -parent $MyInvocation.MyCommand.Path
Import-Module $ScriptDir\Show-ADGroupTreeViewMembers -Force



$global:group_name_arr = [System.Collections.ArrayList]::new()
function add_group_members_to_list {
    [CmdletBinding()]
    param (
        [string] $group_name
    )

    $ButtonType = [System.Windows.Forms.MessageBoxButtons]::OK

    $MessageIcon = [System.Windows.Forms.MessageBoxIcon]::Information

    $MessageBody = "inside getAllGroupUsers"

    $MessageTitle = "Title"

    $Result = [System.Windows.Forms.MessageBox]::Show($MessageBody,$MessageTitle,$ButtonType,$MessageIcon)



    $name_list = [System.Collections.ArrayList]::new()
    $csv_file_name = Split-Path -parent $ScriptDir
    $csv_file_name = Split-Path -parent $csv_file_name
    $csv_file_name = $csv_file_name + "\AD_Groups\"+$group_name+".csv"
    Write-Host "-------------"
    Write-Host $csv_file_name
    Write-Host "-------------"

    if(-not(Test-Path -Path $csv_file_name -PathType Leaf)){
        Set-Content $csv_file_name -Value $null
        $group_members = Get-ADGroupMember -Identity $group_name | Select-Object Name,objectClass
        foreach($name in $group_members){
            
            if($name.objectClass -eq "group"){
                if ($global:group_name_arr -contains $name.Name){
                    Write-Host "loop found $($name.Name) - skipping it"
                    #return $name_list
                } else {
                    $null = $global:group_name_arr.Add($name.Name)
                    Write-Host "entering $($name.Name) sub-group"
                    $return_list = [System.Collections.ArrayList]::new()
                    $return_list =  add_group_members_to_list($name.Name)
                    foreach($return_name in $return_list){
                        if(!($name_list -contains $return_name)){
                            $null = $name_list.Add($return_name)
                            #$return_name | Add-Content $csv_file_name
                            [System.IO.File]::AppendAllText($csv_file_name,$return_name)
                            [System.IO.File]::AppendAllText($csv_file_name,"
")
                        }
                    }
                }
            } elseif ($name.objectClass -eq "user") {
                if(!($name_list -contains $name.Name)){
                    $null = $name_list.Add($name.Name)
                    #$name.Name | Add-Content $csv_file_name
                    [System.IO.File]::AppendAllText($csv_file_name,$name.Name)
                    [System.IO.File]::AppendAllText($csv_file_name,"
")
                }
            } else {
                Write-Output "$($name.Name) - unknown name type $name.objectClass"
            }
        }
    }
    return $name_list
}

##$initial_group_name = "tUsr-AccountsResetPassword"
##$final_list = [System.Collections.ArrayList]::new()
##$final_list = add_group_members_to_list($initial_group_name)

#Write-Output $global:name_arr

##Write-Output "Total user count = $($final_list.Count)"
##Show-ADGroupTreeViewMembers -GroupName $initial_group_name > ".\AD_Groups\$($initial_group_name)_TREE.txt"



#Set-Content "./Pilot_IE_full_user.csv" -Value $null
#Foreach ($name in $global:name_arr) {
#    #Write-Output $name
#    $name | Add-Content "./Pilot_IE_full_user.csv"
#}

