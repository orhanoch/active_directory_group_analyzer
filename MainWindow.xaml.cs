﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Collections.ObjectModel;
using System.Collections;
using System.Reflection;
using System.Windows.Controls.Primitives;
using System.Security.Cryptography.X509Certificates;
using System.Xml.Linq;
using System.CodeDom.Compiler;
using System.Runtime.CompilerServices;

using System.ComponentModel;
using System.Windows.Threading;
using System.Threading;
using System.Windows.Automation.Peers;
using System.Diagnostics;

namespace DotNetMirror.Converters
{
    public class VisibilityConverter : IValueConverter
    {
        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool visibility = false;
            if (value is bool)
            {
                visibility = (bool)value;
            }
            return visibility ? Visibility.Visible : Visibility.Collapsed;
        }
        public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Visibility visibility = (Visibility)value;
            return (visibility == Visibility.Visible);
        }
    }
}


namespace AD_Groups_CSharp
{
    public class TreeMember
    {
        public TreeMember()
        {
            this.members = new ObservableCollection<TreeMember>();
        }

        public string name { get; set; }
        public string backgroundColor { get; set; }
        public int totalMemberCount { get; set; } //-1 if this is a user and not a group
        public int directMemberCount { get; set; }
        public bool isLoop { get; set; }
        public bool isGroup { get; set; }
        public bool isUser { get; set; }
        //public bool isVisible { get; set; }
        public ObservableCollection<TreeMember> members { get; set; }
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<String> groupNameList = new List<String>();   
        private TreeMember rootTreeGroup;
        private static String currentDirectory = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
        
        //for programming
        //private static String ADGroupsDirectory = System.IO.Path.Combine(System.IO.Directory.GetParent(System.IO.Directory.GetParent(currentDirectory).ToString()).ToString(), "AD_Groups");
        
        //for production
        private static String ADGroupsDirectory = System.IO.Path.Combine(Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%"), "AD_Groups");


        private bool? showUsers = false;

        String rootGroupName;
        public static String searchStatus { get; set; }//= "Not Started";


        public delegate void NextGroupDelegate();

        PowerShell ps; 
        public MainWindow()
        {
            // Create an initial default session state.
            var iss = InitialSessionState.CreateDefault2();
            // Set its script-file execution policy (for the current session only).
            iss.ExecutionPolicy = Microsoft.PowerShell.ExecutionPolicy.Bypass;
            ps = PowerShell.Create(iss);
            ps.AddScript("if (-Not (Get-Module -ListAvailable -Name ActiveDirectory)) {throw}");
            try
            {
                ps.Invoke();
            }
            catch
            {
                MessageBox.Show("You need to have the PowerShell Active Directory Module installed for this program to work.\n\n" +
                    "Please open PowerShell as Administrator and run the following command:\n\n" +
                    "Add-WindowsCapability -Online -Name Rsat.ActiveDirectory.DS-LDS.Tools~~~~0.0.1.0");
                Application.Current.Shutdown();
            }

            if (!Directory.Exists(ADGroupsDirectory))
            {
                System.IO.Directory.CreateDirectory(ADGroupsDirectory);
            }

            TreeView1 = new TreeView();
            InitializeComponent();
            searchStatus = "Not Started";
            progressBar.Visibility = Visibility.Collapsed;

            ps.Commands.Clear();
            const string script = @"function wrapper($groupName) {Get-ADGroupMember -Identity $groupName | Select-Object Name,objectClass}";
            ps.AddScript(script);
            ps.Invoke();
            ps.Commands.Clear();
            ps.AddCommand("wrapper").AddParameter("groupName", "AD_Group_Analyzer_TestGroup1");
            ps.Invoke();
        }

        private bool updateTreeMember(TreeMember parentGroup, String name, bool isLoop, bool isGroup, String backgroundColor)
        {
            foreach (TreeMember member in parentGroup.members)
            {
                if (member.name == name)
                {
                    member.isLoop = isLoop;
                    member.backgroundColor = backgroundColor;
                    member.isGroup = isGroup;
                    member.isUser = !isGroup;
                    return true;
                }
                if (member.totalMemberCount > 0)
                {
                    if (updateTreeMember(member, name, isLoop, isGroup, backgroundColor))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private ArrayList addGroupMembersToList(String groupName, String saveDir, TreeMember parentGroup)
        {
            Console.WriteLine("Entered group: " + groupName);
            ArrayList nameList = new ArrayList();

            Console.WriteLine("search status: " + searchStatus);
            if (searchStatus == "StopSignaled" || searchStatus == "Stopped")
            {
                Console.WriteLine("Stopped");
                searchStatus = "Stopped";
                groupNameList = new List<String>();
                Thread.CurrentThread.Abort();
                //return nameList;
            }

            var csv = new StringBuilder();            
            String csvFilePath = System.IO.Path.Combine(saveDir, groupName + ".csv");

            if (!File.Exists(csvFilePath))
            {
                Collection<PSObject> groupMembersResult;

                try
                {
                    ps.Commands.Clear();
                    ps.AddCommand("wrapper").AddParameter("groupName", groupName);
                    groupMembersResult = ps.Invoke();
                }
                catch (Exception e)
                {
                    MessageBox.Show("There was a problem running Get-ADGroupMember in PowerShell.\nThe exception in C# is:\n" + e, "Error");
                    return nameList;
                }


                foreach (PSObject member in groupMembersResult)
                {
                    String memberType = (String)member.Properties["objectClass"].Value;
                    String memberName = (String)member.Properties["Name"].Value;

                    if (memberType == "group")
                    {
                        bool groupExists = false;
                        foreach (String existingGroupName in groupNameList)
                        {
                            if (existingGroupName == memberName)
                            {
                                Console.WriteLine("loop found " + memberName + " - skipping it");
                                //updateTreeMember(rootTreeGroup, existingGroupName, true, true, "Red");
                                parentGroup.members.Add(new TreeMember() { name = memberName, isLoop = true, isGroup = true, isUser=false, backgroundColor = "Red", totalMemberCount = 0, directMemberCount = 0 });
                                groupExists = true;
                            }
                        }
                            
                        if (!groupExists)
                        {
                            TreeMember currGroup = new TreeMember() { name = memberName, isLoop = false, isGroup = true, isUser=false, backgroundColor = "Transparent", totalMemberCount = 0, directMemberCount = 0 };
                            parentGroup.members.Add(currGroup);
                            groupNameList.Add(memberName);
                            Console.WriteLine("entering " + memberName + " sub-group");
                            ArrayList returnList = addGroupMembersToList(memberName, saveDir, currGroup);
                            foreach(String returnName in returnList)
                            {
                                if (!(nameList.Contains(returnName)))
                                {
                                    parentGroup.totalMemberCount++;
                                    nameList.Add(returnName);
                                    csv.AppendLine(returnName);
                                }
                            }
                        }
                    }
                    else if (memberType == "user")
                    {
                        bool duplicateMember = false;
                        foreach (TreeMember parentGroupMember in parentGroup.members)
                        {
                            if (parentGroupMember.name == memberName)
                            {
                                duplicateMember = true;
                            }
                        }
                        if (!duplicateMember)
                        {
                            parentGroup.members.Add(new TreeMember() { name = memberName, isLoop = false, isGroup = false, isUser=true, backgroundColor = "Transparent", totalMemberCount = -1, directMemberCount = -1 });
                            parentGroup.directMemberCount++;
                            if (!(nameList.Contains(memberName)))
                            {
                                parentGroup.totalMemberCount++;
                                nameList.Add(memberName);
                                csv.AppendLine(memberName);
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine(memberName + " - unknown name type " + memberType);
                    }
                }
            }
            File.WriteAllText(csvFilePath, csv.ToString());
            if (groupName == rootGroupName)
            {
                if (searchStatus == "Running")
                {
                    searchStatus = "Done";
                } else if (searchStatus == "StopSignaled")
                {
                    searchStatus = "Stopped";
                }
                groupNameList = new List<String>();
            }
            return nameList;
        }

        private void printTree(TreeMember treeRoot)
        {
            TreeView1.Items.Clear();
            TreeView1.Items.Add(treeRoot);
        }

        private TreeMember getGroupOnlyTree(TreeMember parentGroup)
        {
            TreeMember groupOnlyTree = new TreeMember() { name = parentGroup.name, isLoop = parentGroup.isLoop, isGroup = parentGroup.isGroup, isUser = parentGroup.isUser, backgroundColor = parentGroup.backgroundColor, totalMemberCount = parentGroup.totalMemberCount, directMemberCount = parentGroup.directMemberCount };

            foreach(TreeMember member in parentGroup.members)
            {
                if (member.isGroup)
                {
                    groupOnlyTree.members.Add(getGroupOnlyTree(member));
                }
            }
            return groupOnlyTree;
        }
        private void userCheckboxChange(object sender, RoutedEventArgs e)
        {
            showUsers = showUsersCB.IsChecked;
            if ((bool)showUsers)
            {
                printTree(rootTreeGroup);
            }
            else
            {
                TreeMember groupOnlyTree = getGroupOnlyTree(rootTreeGroup);
                printTree(groupOnlyTree);
            }            
        }

        private void searchButtonAction()
        {
            
            System.IO.DirectoryInfo di = new DirectoryInfo(ADGroupsDirectory);
            rootGroupName = SearchBox.Text;


            foreach (DirectoryInfo dir in di.GetDirectories())
            {

                if (dir.Name.Equals(rootGroupName))
                {
                    dir.Delete(true);
                }
            }

            String saveDir;
            try
            {
                saveDir = System.IO.Path.Combine(ADGroupsDirectory, rootGroupName);

            }
            catch (ArgumentException e)
            {
                MessageBox.Show("Bad group name provided. Error is\n" + e, "Error");
                return;
            }
            System.IO.Directory.CreateDirectory(saveDir);


            rootTreeGroup = new TreeMember() { name = rootGroupName, isLoop = false, isGroup = true, isUser = false, backgroundColor = "Transparent", totalMemberCount = 0, directMemberCount = 0 };
            groupNameList.Add(rootGroupName);
            Task.Run(() => addGroupMembersToList(rootGroupName, saveDir, rootTreeGroup));
        }
        private async void searchButtonOnClick(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Search Button Clicked. searchStatus when clicked was " + searchStatus);
            if (searchStatus != "Not Started")
            {
                MessageBox.Show("Search already running, please wait for it to finish before starting a new one", "Error");
                return;
            }
            searchStatus = "Running";

            showUsersCB.IsEnabled = false;
            TreeView1.Items.Clear();

            progressBar.Visibility = Visibility.Visible;

            //AD_Group_Analyzer_TestGroup1
            await SearchButton.Dispatcher.BeginInvoke(DispatcherPriority.Background, new NextGroupDelegate(searchButtonAction));
            while (searchStatus == "Running" || searchStatus == "StopSignaled")
            {
                Thread.Sleep(10);
                await System.Windows.Threading.Dispatcher.Yield();
            }
            progressBar.Visibility = Visibility.Collapsed;

            if (searchStatus == "Done")
            {
                Console.WriteLine("All done. Printing results now...");
                if ((bool)showUsersCB.IsChecked)
                {
                    printTree(rootTreeGroup);
                }
                else
                {
                    TreeMember groupOnlyTree = getGroupOnlyTree(rootTreeGroup);
                    printTree(groupOnlyTree);
                }
            }
            
            showUsersCB.IsEnabled = true;

            searchStatus = "Not Started";

            Console.WriteLine("Search Button Clicked Done");
        }

        public void searchButtonOnEnter(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                SearchButton.RaiseEvent(new RoutedEventArgs(ButtonBase.ClickEvent));
            }
        }

        private void stopButtonOnClick(object sender, RoutedEventArgs e)
        {
            if (searchStatus == "Running")
            {
                searchStatus = "StopSignaled";
            }
        }

        public void goToExcelFiles(object sender, RoutedEventArgs e)
        {
            Process.Start(@ADGroupsDirectory);
        }
    }
}
